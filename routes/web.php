<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

	$t0 = 21;        // from hour (inclusive) -- int, 0-23 19
    $t1 = 0;         // till hour (excluding) -- int, 0-23 0
    $t  = date('G'); // current hour (derived from current time) -- int, 0-23
	$d = date('Ymd'); //current date
    if ($t0 <= $t1 && $t < $t1 && $d == 20180908) {
        Route::get('thanks', function () {
			return view('late');
		});
    } else {
        Route::get('thanks', function () {
			return view('thanks');
		});
    }

Route::get('/', function () {
    return view('index');
});

Route::get('index', function () {
    return view('index');
});

Route::get('kek', function () {
    return view('entry');
});

Route::get('location', function () {
    return view('location');
});

Route::get('dinner', function () {
    return view('dinner');
});

Route::get('clothes', function () {
    return view('clothes');
});

Route::get('driver', function () {
    return view('driver');
});

Route::get('quiz', function () {
    return view('quiz');
});

Route::get('photo', function () {
    return view('photo');
});

