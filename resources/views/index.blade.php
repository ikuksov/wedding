<!doctype html>
<!--
  Material Design Lite
  Copyright 2015 Google Inc. All rights reserved.

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      https://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License
-->
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="Свадебный сайт Игоря и Анны. Торжество 8 сентября 2018 года.">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">
    <title>Свадьба Игоря и Анны</title>

    <!-- Add to homescreen for Chrome on Android -->
    <meta name="mobile-web-app-capable" content="yes">
    <link rel="icon" sizes="192x192" href="/images/android-desktop.png">

    <!-- Add to homescreen for Safari on iOS -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-title" content="Material Design Lite">
    <link rel="apple-touch-icon-precomposed" href="/images/ios-desktop.png">

    <!-- Tile icon for Win8 (144x144 + tile color) -->
    <meta name="msapplication-TileImage" content="/images/touch/ms-touch-icon-144x144-precomposed.png">
    <meta name="msapplication-TileColor" content="#3372DF">

    <link rel="shortcut icon" href="/images/favicon.png">

    <!-- SEO: If your mobile URL is different from the desktop URL, add a canonical link to the desktop page https://developers.google.com/webmasters/smartphone-sites/feature-phones -->
    <!--
    <link rel="canonical" href="http://www.example.com/">
    -->

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:regular,bold,italic,thin,light,bolditalic,black,medium&amp;lang=en">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="https://code.getmdl.io/1.3.0/material.grey-orange.min.css">
    <link rel="stylesheet" href="/css/styles.css">
    <style>
    #view-source {
      position: fixed;
      display: block;
      right: 0;
      bottom: 0;
      margin-right: 40px;
      margin-bottom: 40px;
      z-index: 900;
    }
    </style>
	<!-- AddEvent script -->
	<script type="text/javascript" src="https://addevent.com/libs/atc/1.6.1/atc.min.js" async defer></script>
	
		
  </head>
  <body>
    <script type="text/javascript">
		var pHour = Date.getHours();
		if ((pHour >= 9) && (pHour < 10))
		{
		  document.getElementById("thanks").style.display = "none";
		  alert(document.getElementById("thanks").style.display);
		}
	</script>
    <div class="demo-blog mdl-layout mdl-js-layout has-drawer is-upgraded">
      <main class="mdl-layout__content">
        <div class="demo-blog__posts mdl-grid">
          <div class="mdl-card coffee-pic mdl-cell mdl-cell--8-col">
            <div class="mdl-card__media mdl-color-text--grey-50">
              <h3><a href="{{ url('location') }}">Торжественная церемония</a></h3>
            </div>
            <div class="mdl-card__supporting-text meta mdl-color-text--grey-600">
              <div class="minilogo"></div>
              <div>
                <strong>Парк отеля Victoria Palace, 15:30</strong>
                <span></span>
              </div>
            </div>
          </div>
          <div class="mdl-card something-else mdl-cell mdl-cell--8-col mdl-cell--4-col-desktop">
            <!--<button class="mdl-button mdl-js-ripple-effect mdl-js-button mdl-button--fab mdl-color--accent">
              <i class="material-icons mdl-color-text--white" role="presentation">add</i>
			  <a target="_blank" href="https://calendar.google.com/event?action=TEMPLATE&amp;tmeid=MjB2cXB2Z2xxN3Q0cGVldjV2MjY0OXNqN2wga3Vrc292Lmlnb3JAbQ&amp;tmsrc=kuksov.igor%40gmail.com"><img border="0" src="https://www.google.com/calendar/images/ext/gc_button1_ru.gif"></a>
              <span class="visuallyhidden">add</span>
            </button>-->
			<div title="Add to Calendar" class="addeventatc">
					Нажмите, чтобы добавить в календарь
					<span class="start">08-09-2018 15:30</span>
					<span class="end">09-09-2018 00:00</span>
					<span class="timezone">Europe/Astrakhan</span>
					<span class="title">Свадьба Игоря и Анны</span>
					<span class="description">Торжественная выездная регистрация</span>
					<span class="location">Красная наб., 3, Астрахань, Астраханская обл., Россия, 414040</span>
			</div>
			
            <div id="minicard" class="mdl-card__media mdl-color--white mdl-color-text--grey-600">
				
			
              
            </div>
            <div class="mdl-card__supporting-text meta meta--fill mdl-color-text--grey-600">
              <div>
                <strong>Вы можете добавить мероприятие в календарь</strong>
              </div>
			  
              <!--<ul class="mdl-menu mdl-js-menu mdl-menu--bottom-right mdl-js-ripple-effect" for="menubtn">
                <li class="mdl-menu__item">About</li>
                <li class="mdl-menu__item">Message</li>
                <li class="mdl-menu__item">Favorite</li>
                <li class="mdl-menu__item">Search</li>
              </ul>
              <button id="menubtn" class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--icon">
                <i class="material-icons" role="presentation">more_vert</i>
                <span class="visuallyhidden">show menu</span>
              </button>-->
            </div>
          </div>
          <div class="mdl-card on-the-road-again mdl-cell mdl-cell--12-col">
            <div class="mdl-card__media mdl-color-text--grey-50">
              <h3><a href="{{ url('dinner') }}">Банкет</a></h3>
            </div>
            <!--<div class="mdl-color-text--grey-600 mdl-card__supporting-text">
              Enim labore aliqua consequat ut quis ad occaecat aliquip incididunt. Sunt nulla eu enim irure enim nostrud aliqua consectetur ad consectetur sunt ullamco officia. Ex officia laborum et consequat duis.
            </div>-->
            <div class="mdl-card__supporting-text meta mdl-color-text--grey-600">
              <div class="minilogo"></div>
              <div>
                <strong>Банкетный комплекс «Palazzo», зал «Alto», 18:00</strong>
                <span></span>
              </div>
            </div>
          </div>
		  <div class="mdl-card on-the-road-again mdl-cell mdl-cell--12-col">
            <div id="clothes" class="mdl-card__media mdl-color-text--grey-50">
              <h3><a href="{{ url('clothes') }}">Одеваемся</a></h3>
            </div>
          <div class="mdl-card__supporting-text meta mdl-color-text--grey-600">
              <div class="minilogo"></div>
              <div>
                <strong>Все что нужно знать о стиле и цвете нашей свадьбы</strong>
                <span></span>
              </div>
            </div>
          </div>
		  <div class="mdl-card on-the-road-again mdl-cell mdl-cell--12-col">
            <div id="driver" class="mdl-card__media mdl-color-text--grey-50">
              <h3><a href="{{ url('driver') }}">Водителям</a></h3>
            </div>
            <div class="mdl-card__supporting-text meta mdl-color-text--grey-600">
              <div class="minilogo"></div>
              <div>
                <strong>Для тех кто будет за рулем в этот день</strong>
                <span></span>
              </div>
            </div>
          </div>
		  <div class="mdl-card on-the-road-again mdl-cell mdl-cell--12-col">
            <div id="thanks" class="mdl-card__media mdl-color-text--grey-50">
              <h3><a href="{{ url('thanks') }}">Благодарности</a></h3>
            </div>
            <div class="mdl-card__supporting-text meta mdl-color-text--grey-600">
              <div class="minilogo"></div>
              <div>
                <strong>Спасибо, что посетили наш свадебный сайт</strong>
                <span></span>
              </div>
            </div>
          </div>
		  <div class="mdl-card on-the-road-again mdl-cell mdl-cell--12-col">
            <div id="quiz" class="mdl-card__media mdl-color-text--grey-50">
              <h3><a href="{{ url('quiz') }}">Конкурс</a></h3>
            </div>
            <div class="mdl-card__supporting-text meta mdl-color-text--grey-600">
              <div class="minilogo"></div>
              <div>
                <strong>Свадебные конкурсы уже начались!</strong>
                <span></span>
              </div>
            </div>
          </div>
		  <div class="mdl-card on-the-road-again mdl-cell mdl-cell--12-col">
		  <iframe width="560" height="315" src="https://www.youtube.com/embed/FYs9EzZsWz4" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
            <!--<div id="photo" class="mdl-card__media mdl-color-text--grey-50">
              <h3><a href="{{ url('photo') }}">Зелёнка</a></h3>
            </div>-->
            <div class="mdl-card__supporting-text meta mdl-color-text--grey-600">
              <div class="minilogo"></div>
              <div>
                <strong>Видео КиноШОУ «Зелёнка»</strong>
                <span></span>
              </div>
            </div>
          </div>
		  <div class="mdl-card on-the-road-again mdl-cell mdl-cell--12-col">
		  <div id="photo_cer" class="mdl-card__media mdl-color-text--grey-50">
              <h3><a href="https://drive.google.com/drive/folders/1cKESh5_yzlIHEK-1V1tUpfuv01FiCGRe?usp=sharing">Фото</a></h3>
            </div>
            <!--<div id="photo" class="mdl-card__media mdl-color-text--grey-50">
              <h3><a href="{{ url('photo') }}">Зелёнка</a></h3>
            </div>-->
            <div class="mdl-card__supporting-text meta mdl-color-text--grey-600">
              <div class="minilogo"></div>
              <div>
                <strong>Здесь вы можете посмотреть фото нашей свадьбы</strong>
                <span></span>
              </div>
            </div>
          </div>
		  <div class="mdl-card on-the-road-again mdl-cell mdl-cell--12-col">
		  <div id="video_ban" class="mdl-card__media mdl-color-text--grey-50">
              <h3><a href="https://drive.google.com/drive/folders/1y9tqQN7TOMKza5Xg_d-YI-YOEJDb9J_L?usp=sharing">Видео</a></h3>
            </div>
            <!--<div id="photo" class="mdl-card__media mdl-color-text--grey-50">
              <h3><a href="{{ url('photo') }}">Зелёнка</a></h3>
            </div>-->
            <div class="mdl-card__supporting-text meta mdl-color-text--grey-600">
              <div class="minilogo"></div>
              <div>
                <strong>Здесь вы можете посмотреть видео нашей свадьбы</strong>
                <span></span>
              </div>
            </div>
          </div>
		  
		  <!--<div class="mdl-card amazing mdl-cell mdl-cell--12-col">
            <div class="mdl-card__title mdl-color-text--grey-50">
              <h3 class="quote"><a href="entry.html">I couldn’t take any pictures but this was an amazing thing…</a></h3>
            </div>
            <div class="mdl-card__supporting-text mdl-color-text--grey-600">
              Enim labore aliqua consequat ut quis ad occaecat aliquip incididunt. Sunt nulla eu enim irure enim nostrud aliqua consectetur ad consectetur sunt ullamco officia. Ex officia laborum et consequat duis.
            </div>
            <div class="mdl-card__supporting-text meta mdl-color-text--grey-600">
              <div class="minilogo"></div>
              <div>
                <strong>The Newist</strong>
                <span>2 days ago</span>
              </div>
            </div>
          </div>
          <div class="mdl-card shopping mdl-cell mdl-cell--12-col">
            <div class="mdl-card__media mdl-color-text--grey-50">
              <h3><a href="entry.html">Shopping</a></h3>
            </div>
            <div class="mdl-card__supporting-text mdl-color-text--grey-600">
              Enim labore aliqua consequat ut quis ad occaecat aliquip incididunt. Sunt nulla eu enim irure enim nostrud aliqua consectetur ad consectetur sunt ullamco officia. Ex officia laborum et consequat duis.
            </div>
            <div class="mdl-card__supporting-text meta mdl-color-text--grey-600">
              <div class="minilogo"></div>
              <div>
                <strong>The Newist</strong>
                <span>2 days ago</span>
              </div>
            </div>
          </div>-->
          <nav class="demo-nav mdl-cell mdl-cell--12-col">
            <div class="section-spacer"></div>
            <a href="{{ url('location') }}" class="demo-nav__button" title="show more">
              Торжественная регистрация
              <button class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--icon">
                <i class="material-icons" role="presentation">arrow_forward</i>
              </button>
            </a>
          </nav>
        </div>
        <footer class="mdl-mini-footer">
          <div class="mdl-mini-footer--left-section">
            <button class="mdl-mini-footer--social-btn social-btn social-btn__twitter">
              <span class="visuallyhidden">Twitter</span>
            </button>
            <button class="mdl-mini-footer--social-btn social-btn social-btn__blogger">
              <span class="visuallyhidden">Facebook</span>
            </button>
            <button class="mdl-mini-footer--social-btn social-btn social-btn__gplus">
              <span class="visuallyhidden">Google Plus</span>
            </button>
          </div>
          <div class="mdl-mini-footer--right-section">
            <button class="mdl-mini-footer--social-btn social-btn__share">
              <i class="material-icons" role="presentation">share</i>
              <span class="visuallyhidden">share</span>
            </button>
          </div>
        </footer>
      </main>
      <div class="mdl-layout__obfuscator"></div>
    </div>
    <!--<a href="https://github.com/google/material-design-lite/blob/mdl-1.x/templates/blog/" target="_blank" id="view-source" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-color--accent mdl-color-text--white">View Source</a>-->
    <script src="https://code.getmdl.io/1.3.0/material.min.js"></script>
  </body>
  <script>
    Array.prototype.forEach.call(document.querySelectorAll('.mdl-card__media'), function(el) {
      var link = el.querySelector('a');
      if(!link) {
        return;
      }
      var target = link.getAttribute('href');
      if(!target) {
        return;
      }
      el.addEventListener('click', function() {
        location.href = target;
      });
    });
  </script>
</html>
