<!doctype html>
<!--
  Material Design Lite
  Copyright 2015 Google Inc. All rights reserved.

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      https://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License
-->
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="Свадебный сайт Игоря и Анны. Торжество 8 сентября 2018 года.">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">
    <title>Свадьба Игоря и Анны</title>

    <!-- Add to homescreen for Chrome on Android -->
    <meta name="mobile-web-app-capable" content="yes">
    <link rel="icon" sizes="192x192" href="images/touch/chrome-touch-icon-192x192.png">

    <!-- Add to homescreen for Safari on iOS -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-title" content="Material Design Lite">
    <link rel="apple-touch-icon-precomposed" href="apple-touch-icon-precomposed.png">

    <!-- Tile icon for Win8 (144x144 + tile color) -->
    <meta name="msapplication-TileImage" content="images/touch/ms-touch-icon-144x144-precomposed.png">
    <meta name="msapplication-TileColor" content="#3372DF">
	
	<link rel="shortcut icon" href="/images/favicon.png">

    <!-- SEO: If your mobile URL is different from the desktop URL, add a canonical link to the desktop page https://developers.google.com/webmasters/smartphone-sites/feature-phones -->
    <!--
    <link rel="canonical" href="http://www.example.com/">
    -->

    <link href='//fonts.googleapis.com/css?family=Roboto:regular,bold,italic,thin,light,bolditalic,black,medium&amp;lang=en' rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
    <link rel="stylesheet" href="https://code.getmdl.io/1.3.0/material.grey-orange.min.css">
    <link rel="stylesheet" href="/css/styles.css">
    <style>
    #view-source {
      position: fixed;
      display: block;
      right: 0;
      bottom: 0;
      margin-right: 40px;
      margin-bottom: 40px;
      z-index: 900;
    }
    </style>
  </head>
  <body>
    <div class="demo-blog demo-blog--blogpost mdl-layout mdl-js-layout has-drawer is-upgraded">
      <main class="mdl-layout__content">
        <div class="demo-back">
          <a class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--icon" href="{{ url('/') }}" title="На главную" role="button">
            <i class="material-icons" role="presentation">arrow_back</i>
          </a>
        </div>
        <div class="demo-blog__posts mdl-grid">
          <div class="mdl-card mdl-shadow--4dp mdl-cell mdl-cell--12-col">
            <div id="thanks_in" class="mdl-card__media mdl-color-text--grey-50">
              <h3>Благодарим Вас</h3>
            </div>
            <div class="mdl-color-text--grey-700 mdl-card__supporting-text meta">
              <div class="minilogo"></div>
              <div>
                <strong>Ваше любимое устройство с которого вы смотрите наш сайт</strong>
                
              </div>
              <div class="section-spacer"></div>
			  <div>
                <span></span>
              </div>
            </div>
            <div class="mdl-color-text--grey-700 mdl-card__supporting-text">
              <p>
                Большое спасибо, что уделили немного своего времени нашему сайту. Надеемся мы смогли учесть большинство интересующих Вас организационных вопросов. Чем ближе мы к нашему счастливому дню свадьбы, тем больше информации будет появляться на нашем сайте. Не забудьте заглянуть сюда накануне 8 сентября, я думаю здесь появится еще больше полезной информации.
              </p>
              <p>
                Огромное спасибо нашим родителям, без них мы бы не смогли воплотить и десятой части задуманного. Крепкого Вам здоровья и спасибо, что вырастили нас такими и мы повстречали друг друга. 
              </p>
			  <p>
			    Отдельную благодарность хочется выразить нашим помощникам, которые помогают нам воплощать наши безумные фантазии в жизнь:
				
				<p><a href="https://vk.com/gospozha_alexia">Эксперт по мамзелевским делам (Антонина Аристова - стилист-имиджмейкер)</a></p>
				<p><img src="/images/ton.jpg"></p>
				<p><a href="https://vk.com/id33541485">Декор со вкусом (Юлия Иванова - декоратор)</a></p>
				<p><img src="/images/decor.jpg"></p>
				<p><a href="https://vk.com/id96955215">Вы сегодня под ее прицелом (Екатерина Ибрагимова - фотограф)</a></p>
				<p><img src="/images/photo.jpg"></p>
				<p><a href="http://vk.com/e_litvinova">Церемония на все 100 (Екатерина Литвинова - церемониймейстер)</a></p>
				<p><img src="/images/ceremony1.jpg"></p>
				<p><a href="https://vk.com/id18145028">Элегантный мужчина (Кирилл Абакумов - ведущий)</a></p>
				<p><img src="/images/ved.jpg"></p>
				<p><a href="https://vk.com/yam1987">Его рука не дрогнет (Алексей Яцуков - видеограф)</a></p>
				<p><img src="/images/video.jpg"></p>
				<p><a href="https://t-do.ru/n.keller">Кому клевый сайт? (Анастасия Хлямина - наставник по WEB)</a></p>
				<p><img src="/images/web.jpg"></p>
				<p>Парням из отдела ЭПО огромный привет!</p>
			  </p>
			  <p>
			  	Отдельное и огромное спасибо Олегу и танцевальной студии "Biales Dance"
			  </p>
			  <p><a href="http://biales.ru/#!/splash"><img src="/images/bial.png" ></a></p>
              <p><a href="https://www.instagram.com/polprist/">Лучший по танцам в этом городе (Олег Поляков)</a></p>
			  <p><img src="/images/dance.jpg"></p>			  
					
			  
			  
			  <p>
			    Обязательно запомните всех этих ребят! Они настоящие профессионалы!
			  </p>
			  
			  <p>
			    Если у Вас остались вопросы, пишите и звоните нам! Мы вам рады! Если вы увидели ошибку на сайте, пожалуйста, напишите жениху (у меня тут интенсив по веб разработке, мог где-то накосячить). 
			  </p>
             
            </div>
            
          </div>

          <nav class="demo-nav mdl-color-text--grey-50 mdl-cell mdl-cell--12-col">
            <a href="{{ url('driver') }}" class="demo-nav__button">
              <button class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--icon mdl-color--white mdl-color-text--grey-900" role="presentation">
                <i class="material-icons">arrow_back</i>
              </button>
              Водителям
            </a>
            <div class="section-spacer"></div>
			<a href="{{ url('quiz') }}" class="demo-nav__button">
              Конкурс
              <button class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--icon mdl-color--white mdl-color-text--grey-900" role="presentation">
                <i class="material-icons">arrow_forward</i>
              </button>
            </a>
          </nav>
        </div>
        <footer class="mdl-mini-footer">
          <div class="mdl-mini-footer--left-section">
            <button class="mdl-mini-footer--social-btn social-btn social-btn__twitter">
              <span class="visuallyhidden">Twitter</span>
            </button>
            <button class="mdl-mini-footer--social-btn social-btn social-btn__blogger">
              <span class="visuallyhidden">Facebook</span>
            </button>
            <button class="mdl-mini-footer--social-btn social-btn social-btn__gplus">
              <span class="visuallyhidden">Google Plus</span>
            </button>
          </div>
          <div class="mdl-mini-footer--right-section">
            <button class="mdl-mini-footer--social-btn social-btn__share">
              <i class="material-icons" role="presentation">share</i>
              <span class="visuallyhidden">share</span>
            </button>
          </div>
        </footer>
      </main>
      <div class="mdl-layout__obfuscator"></div>
    </div>
    <!--<a href="https://github.com/google/material-design-lite/blob/mdl-1.x/templates/blog/" target="_blank" id="view-source" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-color--accent mdl-color-text--white">View Source</a>-->
    <script src="https://code.getmdl.io/1.3.0/material.min.js"></script>
  </body>
</html>
